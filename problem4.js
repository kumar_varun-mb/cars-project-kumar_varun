function problem4(cars){
    if(!cars)
    return 'Invalid Input';
    let years=[];
    for(let i=0;i<cars.length;i++){
        years.push(cars[i].car_year);
    }
    return years;
}
module.exports=problem4;
