function problem3(cars){
    if(!cars)
    return 'Invalid Input';
    else{
    let models=[];
    for(let i=0;i<cars.length;i++){
        models.push(cars[i].car_model);
    }
    return models.sort();
}
}
module.exports=problem3;