function problem1(cars, id){
    if(!cars || !id || id>=cars.length)
        console.log('Invalid Input');
    else{
        for(let i=0;i<cars.length;i++){
            if(cars[i].id==id){
                console.log('Car '+ id+ ' is a ' +cars[i].car_year +" "+ cars[i].car_make+ " "+ cars[i].car_model);
                break;
            }
        }
    }
}
module.exports = problem1;