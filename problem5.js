const problem4 = require('./problem4.js');
function problem5(cars){
    if(!cars)
    console.log('Invalid Input');
    else{
    let old_cars=[];
    let car_years= problem4(cars);
    // console.log(car_years);
    for(let i=0;i<car_years.length;i++){
        if(car_years[i]<2000){
            old_cars.push(cars[i]);
        }
    }
    console.log(old_cars.length+ " cars older than 2000.");
    console.log(old_cars);
}
}
module.exports=problem5;