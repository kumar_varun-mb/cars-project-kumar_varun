function problem2(cars){
    if(!cars)
    console.log('Invalid Input');
    else
    console.log("Last car is a "+cars[cars.length-1].car_make+" "+cars[cars.length-1].car_model);
}
module.exports=problem2;