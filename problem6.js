const cars = require("./cars.js");

function problem6(cars){
    if(!cars){
    return 'Invalid Input';
    }
    else {    
        let res=[];
        for(let i=0;i<cars.length;i++){
            if(cars[i].car_make=="Audi" || cars[i].car_make=="BMW"){ 
                res.push(cars[i]);
        }
        }
        return JSON.stringify(res);
    }
}
module.exports=problem6;